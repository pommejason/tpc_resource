<h1 align="center">开源三方库资源汇总</h1> 

##### 本文收集了一些已经发布在OHPM(OpenHarmony三方组件中心仓)上的三方组件资源，欢迎应用开发者参考和使用，同时也欢迎开发者将自己开源在OHPM上的三方组件，提PR补充到列表当中。

## <a name="三方组件JS/ArkTS"></a>JS/ArkTS语言

### <a name="ui-自定义控件JS/ArkTS"></a>UI
- [@ohos/pulltorefresh](https://gitee.com/openharmony-sig/PullToRefresh) - 支持设置内置动画的各种属性，支持设置自定义动画的下拉刷新、上拉加载组件

- [@ohos/textlayoutbuilder](https://gitee.com/openharmony-sig/TextLayoutBuilder) - TextLayoutBuilder是一个可定制任意样式的文本构建工具，包括字体间距、大小、颜色、布局方式、富文本高亮显示等

- [@ohos/overscroll-decor](https://gitee.com/openharmony-sig/overscroll-decor) - UI滚动组件

- [@ohos/mpchart](https://gitee.com/openharmony-sig/ohos-MPChart) - mpchart是一个包含各种类型图表的图表库，主要用于业务数据汇总，例如销售数据走势图，股价走势图等场景中使用，方便开发者快速实现图表UI，mpchart主要包括曲线图、柱形图、饼状图、蜡烛图、气泡图、雷达图等自定义图表库

- [@ohos/material-dialogs](https://gitee.com/openharmony-sig/material-dialogs) - 是自定义对话框库

- [@ohos/materialprogressbar](https://gitee.com/openharmony-sig/MaterialProgressBar) - 是一个自定义ProgressBar效果的库

- [@ohos/roundedimageview](https://gitee.com/openharmony-sig/RoundedImageView) - RoundedImageView支持圆角（和椭圆或圆形）的快速 ImageView，它支持许多附加功能，包括椭圆、圆角矩形、ScaleTypes 和 TileModes


### <a name="动画JS/ArkTS"></a>动画
- [@ohos/lottie](https://gitee.com/openharmony-tpc/lottieETS) - 适用于OpenHarmony的动画库，功能类似于Java组件lottie、AndroidViewAnimations、Leonids等库

- [@ohos/svg](https://gitee.com/openharmony-sig/ohos-svg) - svg是一个SVG图片的解析器和渲染器，可以解析SVG图片并渲染到页面上，还可以动态改变SVG的样式


### <a name="网络-JS/ArkTS"></a>网络
- [@ohos/axios](https://gitee.com/openharmony-sig/axios) - 一个基于 promise 的网络请求库，可以运行 node.js 和浏览器中。本库基于Axios 原库进行适配，使其可以运行在 OpenHarmony，并沿用其现有用法和特性

### <a name="图片JS/ArkTS"></a>图片
- [@ohos/imageknife](https://gitee.com/openharmony-tpc/ImageKnife) - 更高效、更轻便、更简单的图像加载缓存库，能力类似java组件glide、disklrucache、glide-transformations、fresco、picasso、uCrop、Luban、pngj、Android-Image-Cropper、android-crop等库

- [@ohos/xmlgraphicsbatik](https://gitee.com/openharmony-tpc/XmlGraphicsBatik) - 用于处理可缩放矢量图形（SVG）格式的图像，例如显示、生成、解析或者操作图像

- [@ohos/gif-drawable](https://gitee.com/openharmony-sig/ohos_gif-drawable) - 基于Canvas进行绘制,支持gif图片相关功能

- [@ohos/subsampling-scale-image-view](https://gitee.com/openharmony-sig/subsampling-scale-image-view) - 视图缩放组件

- [@ohos/imageviewzoom](https://gitee.com/openharmony-sig/ImageViewZoom) - ImageViewZoom 支持加载 Resource 或 PixelMap 图片，支持设置图像显示类型功能，支持缩放功能，支持平移功能，双击放大功能，可以监听图片大小，资源变化事件，支持清除显示图片功能



### <a name="多媒体JS/ArkTS"></a>多媒体
- [@ohos/ijkplayer](https://gitee.com/openharmony-sig/ohos_ijkplayer) - 一款基于FFmpeg的视频播放器


### <a name="数据存储JS/ArkTS"></a>数据存储
- [@ohos/disklrucache](https://gitee.com/openharmony-sig/ohos_disklrucache) - 专门为OpenHarmony打造的一款磁盘缓存库，通过LRU算法进行磁盘数据存取

- [@ohos/fileio-extra](https://gitee.com/openharmony-sig/ohos_fileio-extra) - 提供了更丰富全面的文件操作功能

- [@ohos/mmkv](https://gitee.com/openharmony-tpc/MMKV) - 一款小型键值对存储框架

- [@ohos/dataorm](https://gitee.com/openharmony-sig/dataORM) - dataORM是一个具有一行代码操作数据库或链式调用,备份、升级、缓存等特性的关系映射数据库

- [@msgpack/msgpack](https://gitee.com/openharmony-tpc/openharmony_tpc_samples/tree/master/msgpack-javascript) - MessagePack是一个非常高效的对象序列化库

- [@ohos/arangojs](https://gitee.com/openharmony-sig/arangojs) - 是一款适用于OpenHarmony环境的ArangoDB数据库javascript版驱动

- [@ohos/protobufjs](https://gitee.com/openharmony-tpc/protobuf/tree/1.x/) - ProtoBuf(protocol buffers) 是一种语言无关、平台无关、可扩展的序列化结构数据的方法，它可用于（数据）通信协议、数据存储等

- [memory-cache](https://github.com/ptarjan/node-cache) - 内存缓存



### <a name="文件数据JS/ArkTS"></a>文件数据与传输

- [@ohos/mqtt](https://gitee.com/openharmony-sig/ohos_mqtt) - 使应用程序能够连接到MQTT代理以发布消息、订阅主题和接收发布的消息。

- [@ohos/liveeventbus](https://gitee.com/openharmony-sig/LiveEventBus) - 消息总线，支持Sticky，支持跨进程，支持跨应用广播

- [@ohos/mbassador](https://gitee.com/openharmony-sig/ohos_mbassador) - 一个发布订阅模式的三方组件

- [eventbusjs](http://gitee.com/openharmony-tpc/openharmony_tpc_samples/tree/master/EventBus) - eventbusjs主要功能是消息订阅发送

- [js-sha256](http://github.com/emn178/js-sha256) - sha-256/sha-224 hash算法

- [hi-base32](http://github.com/emn178/hi-base32) - base32 encode/decode

- [js-md5](http://github.com/emn178/js-md5) - A simple MD5 hash function for JavaScript supports UTF-8 encoding.

- [js-sha1](http://github.com/emn178/js-sha1) - A simple SHA1 hash function for JavaScript supports UTF-8 encoding.

- [js-md2](http://github.com/emn178/js-md2) - A simple MD2 hash function for JavaScript supports UTF-8 encoding.

- [brotli-js](http://gitee.com/openharmony-sig/brotli) - 一种通用无损压缩算法

- [cbor-js](https://gitee.com/openharmony-tpc/openharmony_tpc_samples/tree/master/cborjsDemo) - 是OpenHarmony系统下使用cbor-js的示例，cbor-js是以简明二进制对象表示 (CBOR) 数据格式 ( RFC8949 )编码和解析数据的Javascript开源库

- [soundex-code](https://gitee.com/openharmony-tpc/commons-codec) - 是一个OpenHarmony系统下使用各种编解码的示例，包含各种格式的简单编码器和解码器， 例如 Base64 Base32 等除了这些广泛使用的编码器和解码器之外，编解码器包还维护了一组语音编码实用程序

- [pako](https://github.com/nodeca/pako) - pako是一个JavaScript库，支持deflate和gzip压缩解压功能

- [lz4js](https://github.com/Benzinga/lz4js) - Lz4js是一个JavaScript库，实现Lz4压缩/解压库

- [snappyjs](https://github.com/zhipeng-jia/snappyjs) - SnappyJS是一个JavaScript库，实现Snappy压缩/解压库

- [@ohos/okio](https://gitee.com/openharmony-tpc/okio) - okio是一个通过数据流、序列化和文件系统来优化系统输入输出流的能力的库


### <a name="安全-JS/ArkTS"></a>安全
- [@ohos/crypto-js](https://gitee.com/openharmony-sig/crypto-js) - 加密算法类库，目前支持MD5、SHA-1、SHA-256、HMAC、HMAC-MD5、HMAC-SHA1、HMAC-SHA256、PBKDF2等


### <a name="工具JS/ArkTS"></a>工具
- [@ohos/zxing](https://gitee.com/openharmony-tpc/zxing) - 一个解析/生成二维码的组件，能力类似java组件zxing，Zbar、zxing-android-embedded、BGAQRCode-Android等

- [js-tokens](http://gitee.com/openharmony-tpc/openharmony_tpc_samples/tree/master/js-tokens) - js-tokens 是一个微型JavaScript的分词器。小巧的、正则表达式驱动的、宽松的、几乎符合规范的 JavaScript 标记器

- [easy-relpace](https://gitee.com/openharmony-tpc/openharmony_tpc_samples/tree/master/Easyrelpace) - 使用Levenshtein 距离算法测量两个字符串之间的差异。 Compare Text替换字符串

- [hex-encode-decode](https://github.com/tiaanduplessis/hex-encode-decode) - Hex encode & decode string

- [text-encoding](https://github.com/inexorabletash/text-encoding) - 在JavaScript中对二进制数据的文本数据和类型化数组缓冲区进行编码和解码

- [qr-code-generator](https://gitee.com/openharmony-sig/qr-code-generator) - 二维码生成器

- [@ohos/juniversalchardet](https://gitee.com/openharmony-sig/juniversalchardet) - 字符编码识别组件

- [@zxing/text-encoding](https://github.com/zxing-js/text-encoding) - 在JavaScript中对二进制数据的文本数据和类型化数组缓冲区进行编码和解码

- [adler-32](https://gitee.com/openharmony-tpc/openharmony_tpc_samples/tree/master/Adler32Demo) - 一个在js环境中实现ADLER-32的校验和算法的三方库

- [dayjs](https://gitee.com/openharmony-tpc/openharmony_tpc_samples/tree/master/dayjs) - dayjs 是一个轻量的处理时间和日期的 JavaScript 库

- [@ohos/jsonschema](https://gitee.com/openharmony-tpc/openharmony_tpc_samples/tree/master/dayjs) - jsonschema是一个轻便易用的JSON模式验证器

- [validator](https://gitee.com/openharmony-tpc/openharmony_tpc_samples/tree/master/validator) - validator.js是字符串验证器和清理器的库

- [percentage-regex](https://gitee.com/openharmony-tpc/openharmony_tpc_samples/tree/master/percentage-regex) - percentage-regex是百分比验证的库

- [leap-year](https://github.com/sindresorhus/leap-year) - 判断闰年的库

- [time-ampm](https://github.com/ipostol/time-ampm) - 获取24小时时间库

- [imagetype](https://github.com/Ackar/node-imgtype) - 获取图片类型库

- [randomColor](https://github.com/davidmerfield/randomColor) - 生成随机颜色的库

- [@ohos/util_code](https://gitee.com/openharmony-tpc/openharmony_tpc_samples/tree/master/utilCode) - utilCode是一个通用工具的示例，包含温度转换、正则校验、图片处理、坐标转换、和颜色获取等常用功能

- [xslt-processor](https://gitee.com/openharmony-tpc/openharmony_tpc_samples/tree/master/xslt-processor) - 支持使用与XML文档配对的XSLT样式表将XML文档转换成多中文本格式（HTML、Text等）的库

- [@ohos/pinyin4js](https://gitee.com/openharmony-tpc/pinyin4js) - 一款汉字转拼音的JavaScript开源库

- [EventEmitter3](https://gitee.com/openharmony-tpc/openharmony_tpc_samples/tree/master/Eventmitter3Demo) - EventEmitter3是一款高性能EventEmitter，支持添加监听事件，监听一次性事件，发送事件，移除事件，统计监听事件的个数，统计监听事件的名称

- [he](https://github.com/mathiasbynens/he) - 支持对字符串进行编解码

- [pcx-js](https://gitee.com/openharmony-tpc/openharmony_tpc_samples/tree/master/pcx-js) - 提供了PCX图像格式解码的能力

- [@isrc/fuse.js](https://gitee.com/pommejason/isrc_fuse.js) - Fuse.js是一款轻量级的JavaScript模糊搜索库，提供了模糊搜索和搜索排序功能




### <a name="其他JS/ArkTS"></a>其他

- [@ohos/arouteronactivityresult](https://gitee.com/openharmony-tpc/arouter-api-onActivityResult) - 用于在各种应用或页面间的跳转和页面间的数据传递

- [ahocorasick](https://gitee.com/openharmony-tpc/openharmony_tpc_samples/tree/master/ahocorasick) - ahocorasick是Aho-Corasick字符串搜索算法的实现，能够高效的进行字符串匹配

- [bignumberjs](https://github.com/MikeMcl/bignumber.js) - A JavaScript library for arbitray-precision decimal and non-decimalarithmetic

- [diff](https://gitee.com/openharmony-tpc/openharmony_tpc_samples/tree/master/jsDiffDemo) - JavaScript文本差异的工具库

- [is-png](https://gitee.com/openharmony-sig/is-png) - is-png是一个判断图片格式的库，根据图片的文件数据，判断图片是否为png格式

- [is-webp](https://gitee.com/openharmony-sig/is-webp) - is-webp是一款根据文件数据，判断图片是否是webp格式的库

- [mustache](https://gitee.com/openharmony-sig/jmustache
  ) - 是mustache模板系统的零依赖实现，通过使用散列或对象中提供的值来扩展模板中的标签

- [leven](https://github.com/sindresorhus/leven) - Measure the difference between two strings using the Levenshtein distance algorithm

- [caverphone](https://github.com/tcort/caverphone) - A JavaScript implementation of the Caverphone 2.0 (aka Caverphone Revised) phonetic matching algorithm

- [metaphone](https://github.com/words/metaphone) - Metaphone phonetic algorithm

- [behaviortree](https://gitee.com/openharmony-tpc/openharmony_tpc_samples/tree/master/behaviorTree) - 是行为树 javascript 版实现

- [@ohos/coap](https://gitee.com/openharmony-tpc/openharmony_tpc_samples/tree/master/behaviorTree) - ohos_coap是基于libcoap v4.3.1版本，封装napi接口，给上层ts提供coap通信能力的三方库

- [gcoord](https://github.com/hujiulong/gcoord) - gcoord(geographic coordinates)是一个处理地理坐标系的JS库，用来修正百度地图、高德地图及其它互联网地图坐标系不统一的问题

- [caverphone](https://gitee.com/openharmony-tpc/openharmony_tpc_samples/tree/master/CaverPhone) - CaverPhone算法(语音匹配算法)的JavaScript实现，规则为：将关键字转换为小写，移除不是a-z的字符，按照规则替换指定字符(如字符串起始、结束，文本中包含cq等)，在结尾放置6个1，返回前十个字符，具体参照CaverPhone算法规则

- [adler-32](https://gitee.com/openharmony-tpc/openharmony_tpc_samples/tree/master/Adler32Demo) - 是一个在js环境中实现ADLER-32的校验和算法能力的库


## <a name="三方组件C_CPP"></a>C/C++语言

[C/C++三方库资源汇总](https://gitee.com/openharmony-sig/tpc_c_cplusplus/blob/master/docs/thirdparty_list.md)


### <a name="音视频C_CPP"></a>音视频
- [vorbis](https://gitee.com/openharmony-sig/vorbis) - 一种通用音频和音乐编码格式组件
- [opus](https://gitee.com/openharmony-sig/opus) - Opus是一个开放格式的有损声音编码格式
- [flac](https://gitee.com/openharmony-sig/flac) - 无损音频编解码器
- [libmp3lame](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/lame) - 是开源mp3编码库，使用MPGLIB解码引擎，专门用于编码 mp3


### <a name="加解密算法C_CPP"></a>加解密算法
- [libogg](https://gitee.com/openharmony-sig/libogg) - 编解码器
- [libsodium](https://gitee.com/hihopeorg/libsodium) - 易用，可移植的加解密库
- [cryptopp](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/cryptopp) - 是密码学库，集成了非常多的密码算法


### <a name="图像处理C_CPP"></a>图像处理
- [stb-image](https://gitee.com/openharmony-sig/stb-image) - C/C++实现的图像解码库
- [pyclipper](https://gitee.com/openharmony-sig/pyclipper) - 图形处理库，可以用于解决平面二维图形的多边形简化、布尔运算和偏置处理
- [jbig2enc](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/jbig2enc) -  是JBIG2文件的编码器
- [leptonica](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/leptonica) -  一个开放源码的C语言库，它被广泛地运用于图像处理和图像分析
- [openjpeg](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/openjpeg) -  是用 C 语言编写的开源 JPEG 2000 编解码器
- [libtiff](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/tiff) -  是一个用来读写标签图片(tiff)的库。该库还支持如下文件格式的转化


### <a name="网络通信C_CPP"></a>网络通信
- [nanopb](https://gitee.com/hihopeorg/nanopb) - 轻量的支持C语言的一种数据协议，可用于数据存储、通信协议等方面
- [c-ares](https://gitee.com/openharmony-sig/c-ares) - 异步解析器库，适用于需要无阻塞地执行 DNS 查询或需要并行执行多个 DNS 查询的应用程序
- [libevent](https://gitee.com/openharmony-sig/libevent) - 事件通知库
- [kcp](https://gitee.com/openharmony-sig/kcp) - ARQ 协议,可解决在网络拥堵情况下tcp协议的网络速度慢的问题
- [mqtt](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/mqtt) - MQTT 是用 C 语言编写的用于MQTT协议的Eclipse Paho C客户端库


### <a name="数据压缩解压C_CPP"></a>数据压缩解压

- [lzma](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/lzma)  \- 是2001年以来得到发展的一个数据压缩算法，它是一种高压缩比的传统数据压缩软件 
- [zstd](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/zstd) -  一种快速的无损压缩算法，是针对 zlib 级别的实时压缩方案，以及更好的压缩比 
- [minizip-ng](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/minizip-ng) \- 一个用C编写的zip文件操作库 
- [unrar](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/unrar) \- 一个解压rar文件的库
- [xz](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/xz) \- 是免费的通用数据压缩软件，具有较高的压缩比


### <a name="文本解析器C_CPP"></a>文本解析器

- [xerces-c](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/xerces-c) - 一个开放源代码的XML语法分析器，它提供了SAX和DOM API
- [rapidjson](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/rapidjson) - 一个跨平台的c++的json的解析器和生成器
- [tinyxml2](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/tinyxml2) - 是 simple、small、efficient 的开源 C++ XML 文件解析库
- [tinyxpath](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/tinyxpath) - 用于从 XML 树中提取 XPath 1.0 表达式


### <a name="编码转换C_CPP"></a>编码转换

- [iconv](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/iconv) - 一个实现字符集转换的库，用于没有Unicode或无法从其他字符转换为Unicode的系统


### <a name="其他工具类C_CPP"></a>其他工具类

- [lua](https://gitee.com/openharmony-sig/lua) - Lua是一种功能强大、高效、轻量级、可嵌入的脚本语言
- [inotify-tools](https://gitee.com/hihopeorg/inotify-tools) - 异步文件系统监控组件，它满足各种各样的文件监控需要，可以监控文件系统的访问属性、读写属性、权限属性、删除创建、移动等操作
- [libharu](https://gitee.com/openharmony-sig/libharu) - 用于生成 PDF格式的文件
- [leveldb](https://gitee.com/openharmony-sig/leveldb) - 快速键值存储库，提供从字符串键到字符串值的有序映射
- [bsdiff](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/bsdiff) - 一个提供二进制文件拆分以及合并能力的三方组件
- [concurrentqueue](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/concurrentqueue) - 一个高效的线程安全的队列的库
- [modbus](https://gitee.com/openharmony-sig/tpc_c_cplusplus/tree/master/thirdparty/modbus) - 是用 C 语言编写的第三方Modbus库来实现modbus通讯，支持 RTU（串行）和 TCP（以太网）通信模式，可根据 Modbus 协议发送和接收数据

