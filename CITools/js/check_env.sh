#!/bin/bash

# Copyright (c) 2023 Huawei Device Co., Ltd.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

ROOT_DIR=$(pwd)

NODE_HOME=$ROOT_DIR/node-v16.15.0-linux-x64
NODE_URL=https://nodejs.org/download/release/v16.15.0/node-v16.15.0-linux-x64.tar.gz
SDK_API9_URL=https://mirrors.huaweicloud.com/openharmony/os/3.2.2/ohos-sdk-windows_linux-public.tar.gz
SDK_API9_SHASUM="edb094e4ce2739a5722ecd6ccddbfea9c8688d46de1144221584e867a8ab11ca9943a3b3d6ad189c553289e091395c719920b96264022a4d47240108f295dee9"
SDK_API10_URL=https://mirrors.huaweicloud.com/openharmony/os/4.0-Beta2/ohos-sdk-windows_linux-public.tar.gz
SDK_API10_SHASUM="1f1d9587b4fcca6e0be4c0c51035dcbbebef43fa38ae735c1946b95163ea05f3c9a58734f8cdad76d516761bf7b846347fc4796413b52defd266f3a25037d682"
SHASUM_FILE=SHA512SUM
API_VERSION=
SDK_API9_DIR=$ROOT_DIR/ohos-sdk-9
SDK9_PKG_NAME=ohos-sdk-9.tar.gz
SDK_API10_DIR=$ROOT_DIR/ohos-sdk-10
SDK10_PKG_NAME=ohos-sdk-10.tar.gz
OHOS_SDK_HOME=
COMPONT_NAME=

echo "ROOT_DIR=$ROOT_DIR"

function prepare_nodejs() {
    echo "prepare nodejs ############"
    if [ ! -d "$NODE_HOME" ]
    then
        wget $NODE_URL
        if [ $? -ne 0 ]
        then
            echo "download nodejs failed!"
            return 1
        fi
        local node_name=${NODE_URL##*/}
        tar -zxf $node_name
        chmod 777 $NODE_HOME/bin/*
    fi
    export PATH=$NODE_HOME:$PATH
    export NODE_HOME=${NODE_HOME}
    export PATH=$NODE_HOME/bin:$PATH
    $NODE_HOME/bin/npm config set @ohos:registry=https://repo.harmonyos.com/npm/
    $NODE_HOME/bin/npm config set lockfile=false
}

function get_openharmony_tpc_samples() {

    local diff_url=$1.diff
    local diff_name=${diff_url##*/}
    local retval=1
    if [ ! -f $diff_name ]
    then
        wget $diff_url
        if [ $? -ne 0 ]
        then
            echo "download $diff_name failed!"
            reutrn $retval
        fi
    fi

    local diff_str=`cat $diff_name | grep "diff --git"`

    for diff in $diff_str
    do

    local tmp=`echo $diff | grep "/"`
    if [ ! -z "$tmp" ]
    then
        tmp=${tmp#*/}
        COMPONT_NAME=$COMPONT_NAME/${tmp%%/*}   
        echo "name=$COMPONT_NAME"
        retval=0
        break
    fi
    done

    return $retval
}

function get_aki_apiversion() {
    cd aki/platform/ohos/publish
    API_VERSION=`cat build-profile.json5 | grep compileSdkVersion`
    echo "$API_VERSION"
    API_VERSION=${API_VERSION##*: }
    if [ "$API_VERSION" == "9," ]
    then
        echo "API9"
        export OHOS_SDK_HOME=$SDK_API9_DIR/linux
    else
        echo "API10"
        export OHOS_SDK_HOME=$SDK_API10_DIR/linux
    fi
    cd $OLDPWD

    export COMPONT_NAME=$COMPONT_NAME

    return 0
}

function get_apiversion(){
    local pr_name=$1
    if [[ $pr_name == *openharmony-tpc* ]]
    then
        pr_name=${pr_name#*openharmony-tpc/}
    else
        pr_name=${pr_name#*openharmony-sig/}
    fi
    
    COMPONT_NAME=${pr_name%/pulls*}
    echo "COMPONT_NAME=$COMPONT_NAME"
    if [ $COMPONT_NAME == "manifest" ]
    then
        echo "*************manifest***************";
        return 0;
    fi

    if [ $COMPONT_NAME == aki ]
    then
        get_aki_apiversion
        return $?
    fi

    if [ $COMPONT_NAME == "openharmony_tpc_samples" ]
    then
        get_openharmony_tpc_samples $1
    fi

    cd $COMPONT_NAME
    API_VERSION=`cat build-profile.json5 | grep compileSdkVersion`
    echo "$API_VERSION"
    API_VERSION=${API_VERSION##*: }
    if [ "$API_VERSION" == "9," ]
    then
        echo "API9"
        export OHOS_SDK_HOME=$SDK_API9_DIR/linux
    else
        echo "API10"
        export OHOS_SDK_HOME=$SDK_API10_DIR/linux
    fi
    cd $OLDPWD

    export COMPONT_NAME=$COMPONT_NAME

    return 0
}

function prepare_ohpm() {
    if [ ! -d ohpm ]
    then
        git clone git@gitee.com:zhong-luping/ohpm_tools.git
        if [ $? -ne 0 ]
        then
            echo "failed to download ohpm tools!!!"
            return 1
        fi
        cp ohpm_tools/ohpm.tar ./
        tar -xf ohpm.tar
        rm -rf ohpm.tar ohpm_tools
    fi
    return 0
}

function check_sdk_sum() {

    if [ "$API_VERSION" == "9," ]
    then
        if [ -d $SDK_API9_DIR ]
        then
            if [ -f "$SDK_API9_DIR/$SHASUM_FILE" ]
            then
                local shasum=`cat $SDK_API9_DIR/$SHASUM_FILE`
                shasum=${shasum:0:128}
                if [ "$shasum" == "$SDK_API9_SHASUM" ]
                then
                    echo "no need change the sdk!!"
                    return 0
                fi
                echo "$shasum != $SDK_API9_SHASUM"
                echo "need change sdk!!"
            fi

            rm -rf $ROOT_DIR/ohos-sdk-9
        fi
    else
        if [ -d $SDK_API10_DIR ]
        then
            if [ -f "$SDK_API10_DIR/$SHASUM_FILE" ]
            then
                local shasum=`cat $SDK_API10_DIR/$SHASUM_FILE`
                shasum=${shasum:0:128}
                if [ "$shasum" == "$SDK_API10_SHASUM" ]
                then
                    echo "no need change the sdk"
                    return 0
                fi
                echo "$shasum != $SDK_API9_SHASUM"
                echo "need change sdk"
            fi

            rm -rf $ROOT_DIR/ohos-sdk-10
        fi
    fi
}

function prepare_sdk(){
    old_dir=`pwd`
    echo "prepare sdk ############################### API_VERSION=$API_VERSION"
    echo "$old_dir"
    
    check_sdk_sum

    if [ "$API_VERSION" == "9," ]
    then
        echo "prepare SDK API 9 ##################"

        if [ ! -d "$SDK_API9_DIR" ]
        then
            if [ ! -f $SDK9_PKG_NAME ]
            then
                curl -L $SDK_API9_URL --output $SDK9_PKG_NAME
                if [ $? -ne 0 ]
                then
                    echo "download sdk api9 failed!"
                    rm -f $SDK9_PKG_NAME
                    return 1
                fi
            fi
            tar -zxf $SDK9_PKG_NAME
            mv ohos-sdk ohos-sdk-9
            sha512sum $SDK9_PKG_NAME > $SDK_API9_DIR/$SHASUM_FILE
            cd $SDK_API9_DIR/linux
            if [ $? -ne 0 ]
            then
                echo "prepare SDK9 failed!"
                return 1
            fi
            mkdir 9
            
            for file in $(ls ./)
            do
                if [ $file != "9" ]
                then
                    mv $file 9/
                    cd 9/
                    unzip $file >> /dev/null
                    cd $OLDPWD
                fi
                echo "$file"
            done
        fi
        cd $SDK_API9_DIR/linux/9/ets/build-tools/ets-loader
        $NODE_HOME/bin/npm install
        if [ $? -ne 0 ]
        then
            echo "ets-loader npm install failed!"
            return 1
        fi

        cd $SDK_API9_DIR/linux/9/js/build-tools/ace-loader
        $NODE_HOME/bin/npm install
        if [ $? -ne 0 ]
        then
            echo "ace-loader npm install failed!"
            return 1
        fi
    else
        echo "prepare SDK API 10 ##################"
        if [ ! -d $SDK_API10_DIR ]
        then
            if [ ! -f $SDK10_PKG_NAME ]
            then
                curl -L $SDK_API10_URL --output $SDK10_PKG_NAME
                if [ $? -ne 0 ]
                then
                    echo "download sdk api10 failed!"
                    rm -f $SDK10_PKG_NAME
                    return 1
                fi
            fi
            tar -zxf $SDK10_PKG_NAME
            mv ohos-sdk ohos-sdk-10
            sha512sum $SDK10_PKG_NAME > $SDK_API10_DIR/$SHASUM_FILE
            cd $SDK_API10_DIR/linux
            if [ $? -ne 0 ]
            then
                echo "prepare SDK10 failed!"
                return 1
            fi
            mkdir 10
            for file in $(ls ./)
            do
                if [ $file != "10" ]
                then
                    mv $file 10/
                    cd 10
                    unzip $file >> /dev/null
                    cd $OLDPWD
                fi
            echo "$file"
            done
        fi
        cd $SDK_API10_DIR/linux/10/ets/build-tools/ets-loader
        $NODE_HOME/bin/npm install
        if [ $? -ne 0 ]
        then
            echo "ets-loader npm install failed!"
            return 1
        fi

        cd $SDK_API10_DIR/linux/10/js/build-tools/ace-loader
        $NODE_HOME/bin/npm install
        if [ $? -ne 0 ]
        then
            echo "ace-loader npm install failed!"
            return 1
        fi
    fi

    cd $old_dir
    return 0
}
